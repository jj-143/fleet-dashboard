# Fleet Dashboard

## Environment

- package manager: Yarn

## Clone the project

`git clone https://gitlab.com/jj-143/fleet-dashboard`

## Install

`yarn install`

## Build

`yarn build`

## Start dev server

`yarn start` and navigate to the server default: localhost:3000
