import {
  Box,
  Button,
  Container,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { DataGrid, GridColDef, GridRenderCellParams } from "@mui/x-data-grid";
import { useQuery } from "@tanstack/react-query";
import { useState } from "react";
import StarButton from "./components/StarButton";
import { Location, getLocations } from "./lib/fleets";

const columns: GridColDef<Location>[] = [
  {
    field: "star",
    headerName: "favorite",
    renderCell: ({ row }) => <StarButton id={row.id} />,
  },
  {
    field: "name",
    headerName: "Locations",
    width: 250,
    renderCell: ({ row }) => (
      <Button
        variant="contained"
        sx={{
          backgroundColor: row.robot?.is_online ? "blue" : "grey",
        }}
      >
        {row.name}
      </Button>
    ),
  },
  {
    field: "robot",
    headerName: "Robots",
    width: 200,
    renderCell: (params: GridRenderCellParams<Location, Location["robot"]>) =>
      params.value ? (
        <>🟢 {params.value?.id}</>
      ) : (
        <>
          🔴 <Button>Add</Button>
        </>
      ),
  },
];

type LocationGroup = "all" | "starred";

function Dashboard() {
  const [locationGroup, setLocationGroup] = useState<LocationGroup>("all");
  const [query, setQuery] = useState("");
  const [paginationModel, setPaginationModel] = useState({
    pageSize: 25,
    page: 0,
  });

  const { data, isLoading } = useQuery(
    ["locations", query, paginationModel.page + 1, locationGroup],
    () =>
      getLocations({
        robot_id: query,
        location_name: query,
        page: paginationModel.page + 1,
        is_starred: locationGroup === "starred",
      }),
    {
      keepPreviousData: true,
      onSuccess: (data) => {
        setPaginationModel({
          ...paginationModel,
          pageSize: data.locations.length,
        });
      },
    }
  );

  return (
    <>
      <Container sx={{ p: 8 }}>
        <Box>
          <Typography variant="h1" sx={{ fontSize: "2rem", mb: 2 }}>
            Your Fleet
          </Typography>
        </Box>
        <Box sx={{ display: "flex", justifyContent: "space-between", mb: 2 }}>
          <Select
            value={locationGroup}
            label="group"
            onChange={(e) => setLocationGroup(e.target.value as LocationGroup)}
          >
            <MenuItem value="all" defaultChecked>
              All Locations
            </MenuItem>
            <MenuItem value="starred">Starred</MenuItem>
          </Select>
          <TextField
            label="Search robot or location"
            variant="outlined"
            value={query}
            onChange={(e) => setQuery(e.target.value)}
          />
        </Box>
        <DataGrid
          rows={data?.locations ?? []}
          columns={columns}
          loading={isLoading}
          rowCount={data?.total_count ?? 0}
          paginationMode="server"
          paginationModel={paginationModel}
          onPaginationModelChange={setPaginationModel}
          checkboxSelection
        />
      </Container>
    </>
  );
}

export default Dashboard;
