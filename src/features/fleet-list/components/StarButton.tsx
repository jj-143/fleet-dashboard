import { Button } from "@mui/material";
import { useQuery } from "@tanstack/react-query";
import { getStarredLocationIds, saveStarredLocation } from "../lib/fleets";

type Props = {
  id: number;
};

function StarButton(props: Props) {
  const { data, refetch } = useQuery(
    ["starred-location-ids"],
    getStarredLocationIds
  );
  const isStarred =
    data?.location_ids.find((id) => id === props.id) !== undefined;

  const onClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    if (isStarred) {
      const newItems = (data?.location_ids ?? []).filter(
        (id) => id !== props.id
      );
      saveStarredLocation(newItems);
    } else {
      const newItems = [...(data?.location_ids ?? []), props.id];
      saveStarredLocation(newItems);
    }

    refetch();
  };

  return <Button onClick={onClick}>{isStarred ? "⭐" : "❌"}</Button>;
}

export default StarButton;
