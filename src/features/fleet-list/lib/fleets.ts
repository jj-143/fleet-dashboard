import axios from "axios";

const client = axios.create({
  baseURL: "",
});

type Params = {
  page?: number;
  robot_id?: string;
  location_name?: string;
  is_starred?: boolean;
};

export type LocationResults = {
  total_count: number;
  locations: Location[];
};

export type Location = {
  id: number;
  name: string;
  robot?: {
    id: string;
    is_online: boolean;
  };
};

export async function getLocations(params: Params): Promise<LocationResults> {
  return (
    await client.get<LocationResults>("/locations", {
      params,
    })
  ).data;
}

export type StarredLocationsResults = {
  location_ids: number[];
};

export async function getStarredLocationIds(): Promise<StarredLocationsResults> {
  return (await client.get<StarredLocationsResults>("/starred_location_ids"))
    .data;
}

export async function saveStarredLocation(
  ids: number[]
): Promise<StarredLocationsResults> {
  return (
    await client.put<StarredLocationsResults>("/starred_location_ids", ids)
  ).data;
}
