import { DefaultBodyType, rest } from "msw";

import { Location, locations } from "./db";

interface LocationsResult {
  total_count: number;
  locations: Location[];
}

interface LocationsPathParams {
  page: string;
  location_name: string;
  robot_id: string;
  is_starred: string;
}

export const handlers = [
  rest.get<DefaultBodyType, LocationsPathParams, LocationsResult>(
    "/locations",
    (req, res, ctx) => {
      const PAGE_SIZE = 6;

      const location_name = req.url.searchParams.get("location_name") ?? "";
      const robot_id = req.url.searchParams.get("robot_id") ?? "";
      const page = Number(req.url.searchParams.get("page") ?? "1");
      const is_starred = req.url.searchParams.get("is_starred") === "true";

      // OR search for "location_name" and "robot_id"
      const filtered = locations.filter(
        (location) =>
          location.name
            .toLocaleLowerCase()
            .includes(location_name.toLocaleLowerCase()) ||
          location.robot.id
            .toLocaleLowerCase()
            .includes(robot_id.toLocaleLowerCase())
      );

      // filter starred

      let starredFiltered = filtered;

      if (is_starred) {
        const location_ids: number[] = JSON.parse(
          sessionStorage.getItem("starred_location_ids") || "[]"
        );

        starredFiltered = filtered.filter((location) =>
          location_ids.find((id) => id === location.id)
        );
      }

      const result: LocationsResult = {
        total_count: starredFiltered.length,
        locations: starredFiltered.slice(
          PAGE_SIZE * (page - 1),
          PAGE_SIZE * page
        ),
      };

      return res(ctx.status(200), ctx.json(result));
    }
  ),

  rest.get("/starred_location_ids", (req, res, ctx) => {
    const location_ids = JSON.parse(
      sessionStorage.getItem("starred_location_ids") || "[]"
    );

    return res(
      ctx.status(200),
      ctx.json({
        location_ids,
      })
    );
  }),

  rest.put("/starred_location_ids", (req, res, ctx) => {
    if (!req.body) {
      return res(
        ctx.status(500),
        ctx.json({ error_msg: "Encountered unexpected error" })
      );
    }

    sessionStorage.setItem("starred_location_ids", JSON.stringify(req.body));

    return res(ctx.status(204));
  }),
];
